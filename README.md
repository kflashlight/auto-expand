Auto-expand v. 1.0
==================

Auto expand multiple textareas on one webpage with Javascript. 

It works out of the box. Just include the auto_resize.js file, and you never have to think about the file again.

It automaticly detects all the textareas and expand or increase them when you type.

NOTE! This is a on-going prosject and not perfect or finished yet. Still not sure if this working for all
older browsers. 

I choose to write this in Javascript to keep it as light-weight and fast as possible.

Contributers are welcome
