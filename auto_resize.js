// KFlash prodution

var d = window,
    e = function () {
        function resize(a) { //Resize the textareas
            a.style.height = "auto";
            a.style.height = a.scrollHeight + (window.opera ? a.offsetHeight + parseInt(window.getComputedStyle(a, null).getPropertyValue("border-top-width")) : a.offsetHeight - a.clientHeight) + "px"
        }

		// Find all textareas on the page, and iterate through them
		
        for (var ta = document.getElementsByTagName("textarea"), i = 0; i < ta.length; i++) ta[i].addEventListener && ta[i].addEventListener("input", function () {
            resize(this)
        }), ta[i].attachEvent && ta[i].addEventListener("keyup", function () {
            resize(this)
        }), ta[i].attachEvent && ta[i].attachEvent("onkeyup",
            function () {
                resize(this)
        }), ta[i].attachEvent && ta[i].addEventListener("mouseup", function () {
            resize(this)
        }), ta[i].attachEvent && ta[i].attachEvent("onmouseup", function () {
            resize(this)
        })
    };
d.addEventListener ? d.addEventListener("load", e, !1) : d.attachEvent ? d.attachEvent("onload", e) : window.alert("Your browser is to old for this. It should be retired.");